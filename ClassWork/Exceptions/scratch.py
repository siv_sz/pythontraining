try:
    value = float(input("Please enter a number: "))
except EOFError:
    print("Sorry, but don't press CNTRL+Something")
    exit()
except ValueError:
    print("Alright... we said a number. Pick any from these: 1234567890. Hint: you can combine them anyway you like")
    exit()
finally:
    
