class BankAccount():
    def __init__(self,name,account):
        self.name = name
        self.account = account

    def withdraw(self,amount):
        self.amount = amount
        result = self.account - amount
        print(f"The account {self.name}, had {self.account} and we charged {self.amount}. The remaining balance is {result}. ")

    def deposit(self,amount):
        self.amount = amount
        result = self.account + amount
        print(f"The account {self.name}, had {self.account} and we deposited {self.amount}. The remaining balance is {result}. ")


maria_account = BankAccount("Maria", 1_300)
pesho_account = BankAccount("Pesho", 100)

print(maria_account)
print(maria_account)

