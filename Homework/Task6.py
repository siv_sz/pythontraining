distances_from_sofia = [
    ("Bansko", 97),
    ("Brussels", 1701),
    ("Alexandria", 1403),
    ("Nice", 1307),
    ("Szeged", 469),
    ("Dublin", 2479),
    ("Palermo", 987),
    ("Moscow", 1779),
    ("Oslo", 2098),
    ("London", 2019),
    ("Madrid", 2259),
]

close_distances= []

for index in range(0,len(distances_from_sofia)):
    element = distances_from_sofia[index]
    if int(element[1]) < 1500:
        close_distances.append(distances_from_sofia[index])
print('Close distances are:',*sorted(close_distances, key=lambda dist: dist[1]),sep='\n ')