def get_user_data():
    """ Input user data from CMD line """
    #initializing an empty Dictionary...
    user_data = {}
    def validate_name(name):
        """ Function to validate the name has at least or more characters. A J Simmons is screwed"""
        while len(name) < 2:
            name = input("Sorry, bro! We've asked for a name, not initials. Try again: ")
        return (name) 
    
    def validate_height(height):
        """ Making sure this is not an alien or Shaqille O'Neal """
        while float(height) > 2.5 or float(height) < 0.5:
            height = input("Please enter your height, no cheating! ")
        return (height)
 
    def validate_weight(weight):
        """ Lets make sure we get no elephants in the room... """
        while int(weight) > 300 or int(weight) < 5:
            print("Alright there, please enter your real weight in kilograms, not miligrams or tonnes!")
            print("See, I live in the RAM so I can't tell you what you should write here...")
            weight = input("but something between 5 and 300 is what others input *wink wink* ")
        return (weight)
    
    user_data['name'] = validate_name(input("Please enter your name: "))
    user_data['height'] = validate_height(input("Please enter height in meters: "))
    user_data['weight'] = validate_weight(input("Please enter weight in kilograms: "))

    return(user_data)


def calculate_BMI(weight,height):
    """ Calculate the BMI and return Fload BMI = w/ (h*h) """
    calc_result = (int(weight)/(float(height)**2))
    return(calc_result)

def calc_BMI_category(bmi):
    """ Calculates the BMI category """
    how_fat_you_are = []
    if   bmi >  30:
        how_fat_you_are = "You're really fat! Like FAT FAT!"
    elif bmi >= 25 :
        how_fat_you_are = "You've got a bit more to love... you're fat."
    elif bmi >  18.5:
        how_fat_you_are = "You just think you're fat. You're fine!"
    else:
        how_fat_you_are = "You skinny man! Go to McDonnalds, now!"
    return(how_fat_you_are)

def centimiters_to_meters(cm):
    centimiters = int(cm)
    """ Converts from centimeters to meters. Like we need this?! """
    meters=(centimiters/100)
    return(meters)

def print_results(bmi_category):
    """ Calling a print.  """
    print('\n')
    print(bmi_category)

user_data = get_user_data()
bmi = calculate_BMI(user_data["weight"],user_data["height"])
bmi_category = calc_BMI_category(bmi)
print_results(bmi_category)